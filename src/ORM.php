<?php
declare(strict_types=1);

namespace Charm;

use Exception;
use Throwable;
use TypeError;
use DateTimeZone;
use Charm\ORM\Base;
use Charm\ORM\Error;
use ReflectionClass;
use DateTimeImmutable;
use DateTimeInterface;
use Charm\Schema\Schema;
use Charm\Context\Context;
use Charm\Error\HttpCodes;
use Charm\OpenApi\OpenApi;
use Charm\DB\Error as DBError;
use Charm\ORM\ValidationError;
use Charm\Table\ModelInterface;
use Charm\Table\TableInterface;
use Charm\ORM\CancelledException;
use Charm\ORM\Util\ObjectReference;
use Charm\ORM\Util\OpenApiValidator;
use Charm\ORM\Util\ReferenceableObject;
use Charm\Event\StaticEventEmitterTrait;
use Charm\Event\StaticEventEmitterInterface;

/**
 * @property int|string|null $id
 */
abstract class ORM extends Base implements ModelInterface, StaticEventEmitterInterface, ReferenceableObject
{
    use StaticEventEmitterTrait;

    /**
     * Event to receive the model definition.
     */
    public const MODEL_DEFINITION_EVENT = self::class.'::MODEL_DEFINITION_EVENT';

    /**
     * Event immediately before the data is saved to the backend.
     */
    public const SAVE_EVENT = self::class.'::SAVE_EVENT';

    /**
     * Event immediately after the data is saved to the backend.
     */
    public const SAVED_EVENT = self::class.'::SAVED_EVENT';

    /**
     * Event immediately before the object is about to be deleted.
     */
    public const DELETE_EVENT = self::class.'::DELETE_EVENT';

    /**
     * Event immediately after the object was deleted.
     */
    public const DELETED_EVENT = self::class.'::DELETED_EVENT';

    public const PERMISSION_DENIED_ERROR = HttpCodes::FORBIDDEN;
    public const CLASS_NOT_FOUND_ERROR = 1001;
    public const OBJECT_NOT_SAVED_ERROR = 1002;
    public const MODEL_DEFINITION_ERROR = 1003;
    public const VALIDATION_ERROR = 1004;
    public const PROPERTY_NOT_FOUND_ERROR = 1005;
    public const TRANSACTION_BOUNDARIES_ERROR = 1006;
    public const READ_ONLY_PROPERTY_ERROR = 1007;
    public const WRITE_ONLY_PROPERTY_ERROR = 1008;
    public const ILLEGAL_OPERATION_ERROR = 1009;
    public const INCORRECT_USAGE_ERROR = 1010;
    public const INTERNAL_ERROR = 1011;
    public const DATABASE_RELATED_ERROR = 1012;

    /**
     * self::TYPE_*
     * ============.
     *
     * The type definitions are structured PHP arrays, modelled after JSON Schema Draft 7
     * Properties starting with '$' are properties with special meaning:
     *
     *   * $extends May contain another type definition which we will copy properties from.
     *   * $requires Is an array like [
     *         'extraPropertyName' => [
     *             'type' => 'string',
     *             'description' => 'Describe the prop'
     *          ],
     *
     * @psalm-type ModelFieldSchema = array{
     *     $path: string,
     *     $extends?: array,
     *     $meta?: array,
     *     type?: string,
     *     minLength?: int,
     *     maxLength?: int,
     *     uniqueItems?: bool,
     *     minimum?: int,
     *     exclusiveMinimum?: bool,
     *     maximum?: int,
     *     exclusiveMaximum?: bool,
     *     readOnly?: bool,
     *     writeOnly?: bool,
     *     default?: mixed,
     *     multipleOf?: float,
     *     nullable?: bool,
     *     enum?: array<mixed>,
     *     format?: string,
     *     pattern?: string
     * }
     *
     * @psalm-type ModelSchema = array{
     *     $path: string,
     *     $meta: array{
     *         plural: string,
     *         plural_description: string,
     *         singular: string,
     *         singular_description: string,
     *         table: string,
     *         auto_increment?: bool
     *     },
     *     required?: array<string>,
     *     properties: array<string, ModelFieldSchema>
     * }
     */
    public const TYPE_PRIMARY_KEY = [
        '$path' => '/components/schemas/Charm-Types-PrimaryKey',
        '$extends' => self::TYPE_STRING,
        '$meta' => [
            'unique' => true,
            'mysql_type' => 'BIGINT UNSIGNED',
        ],
        'readOnly' => true,
        'example' => '123456789',
    ];
    public const TYPE_STRING = [
        '$path' => '/components/schemas/Charm-Types-String',
        '$meta' => [
            'type' => 'string',
            'mysql_type' => 'VARCHAR',
        ],
        'type' => 'string',
        'example' => 'A string',
    ];
    public const TYPE_BOOL = [
        '$path' => '/components/schemas/Charm-Types-Boolean',
        '$meta' => [
            'type' => 'bool',
            'mysql_type' => 'TINYINT',
        ],
        'type' => 'boolean',
        'example' => true,
    ];
    public const TYPE_INT = [
        '$path' => '/components/schemas/Charm-Types-Integer',
        '$meta' => [
            'type' => 'int',
            'mysql_type' => 'BIGINT',
        ],
        'type' => 'integer',
        'example' => 42,
    ];
    public const TYPE_FLOAT = [
        '$path' => '/components/schemas/Charm-Types-Float',
        '$meta' => [
            'type' => 'float',
            'mysql_type' => 'FLOAT',
        ],
        'type' => 'number',
        'example' => 31.337,
    ];
    public const TYPE_DATETIME = [
        '$path' => '/components/schemas/Charm-Types-DateTime',
        '$meta' => [
            'type' => 'datetime',
            'mysql_type' => 'DATETIME',
        ],
        'type' => 'string',
        'format' => 'date-time',
        'example' => '2018-11-13T20:20:39+00:00',
    ];
    public const TYPE_FOREIGN_KEY = [
        '$path' => '/components/schemas/Charm-Types-ForeignKey',
        '$extends' => self::TYPE_PRIMARY_KEY,
        '$meta' => [
            'unique' => false,
            '$requires' => [
                '$model' => [
                    'type' => 'string',
                    'description' => 'The class name of the model that this field is a foreign key for',
                ],
            ],
        ],
        'readOnly' => false,
    ];
    public const TYPE_JSON = [
        '$path' => '/components/schemas/Charm-Types-Json',
        '$meta' => [
            'type' => 'json',
            'mysql_type' => 'JSON',
        ],
        'type' => 'string',
        'format' => 'json',
        'example' => [
            'a_key' => 'Some value',
            'another_key' => 'Another value',
        ],
    ];

    public const TYPE_ENUM = [
        '$path' => '/components/schemas/Charm-Types-Enum',
        '$meta' => [
            'type' => 'enum',
            'mysql_type' => 'ENUM',
        ],
        '$requires' => [
            'enum' => [
                'type' => 'array',
                'description' => 'An array of legal values for the enum',
            ],
        ],
        'type' => null,
    ];
    // requires 'values' => ['val', 'val2', ...]
    public const TYPE_DECIMAL = [
        '$path' => '/components/schemas/Charm-Types-Decimal',
        '$meta' => [
            'type' => 'decimal',
            'mysql_type' => 'DECIMAL',
            // This type requires '$precision' and '$scale' to be defined.
            '$requires' => [
                '$precision' => [
                    'type' => 'integer',
                    'description' => 'The number of decimal digits to allow in total',
                ],
                '$scale' => [
                    'type' => 'integer',
                    'description' => 'How many of these digits belong after the decimal point',
                ],
            ],
        ],
        'pattern' => '^([0-9]|[1-9][0-9]*)(\.[0-9]+)?$',
        'type' => 'string',
        'example' => '49.99',
    ];

    public const TYPE_CLASS_NAME = [
        '$path' => '/components/schemas/Charm-Types-ClassName',
        '$instanceOf' => null,
        '$extends' => self::TYPE_STRING,
        'format' => 'class-name',
        '$meta' => [
        ],
    ];

    public const TYPE_CURRENCY = [
        '$path' => '/components/schemas/Charm-Types-Currency',
        '$extends' => self::TYPE_DECIMAL,
        '$precision' => 15,
        '$scale' => 3,
    ];

    /**
     * Override this method to do customize object initialization.
     *
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    private static int $elevatedPrivilegesCounter = 0;

    /**
     * Some operations are privileged when working on the data model. By
     * doing the work from inside a callable passed to this function, you
     * can bypass privilege restrictions.
     *
     * WARNING! Don't stay privileged for too long; do only what's needed.
     *
     * @return mixed
     */
    final public static function elevatePrivileges(callable $process)
    {
        ++static::$elevatedPrivilegesCounter;
        try {
            $result = $process();
        } catch (Throwable $e) {
            throw $e;
        } finally {
            --static::$elevatedPrivilegesCounter;
        }

        return $result;
    }

    /**
     * Perform a set of operations in a serializable transaction. If accessing dirty properties on objects,
     * an exception is thrown and the transaction will fail.
     *
     * WARNING! To avoid deadlocks, try to load rows from the database in a consinstent order. If one
     * request fetches a row from 'users' and then another row from 'shopping_cart' and a parallel request
     * fetches 'shopping_cart' and then 'users' - you could potentially cause a race condition. Both requests
     * should fetch 'users' first, then 'shopping_carts', or vice versa.
     *
     * @return mixed
     */
    final public static function transaction(callable $process)
    {
        $ctx = Context::instance();
        if (!isset($ctx->ormTransactionObjects)) {
            $ctx->ormTransactionObjects = [];
        }
        $db = DB::instance();
        $db->beginTransaction();
        $exception = null;
        try {
            $result = $process();
            $db->commit();
        } catch (Throwable $e) {
            $exception = $e;
        }
        if ($db->getTransactionDepth() === 0) {
            // We are no longer inside a transaction, so we'll promote the objects loaded into transactionless objects
            foreach ($ctx->ormTransactionObjects as $object) {
                $object->data['transaction_tag'] = null;
            }
            unset($ctx->ormTransactionObjects);
        }
        if (null !== $exception) {
            throw $exception;
        }

        return $result ?? null;
    }

    /**
     * Function to check if privileges has been escalated.
     */
    final public static function isPrivileged(): bool
    {
        return static::$elevatedPrivilegesCounter > 0;
    }

    /**
     * Override this method to control who is allowed to create new objects.
     */
    public static function canCreate(): bool
    {
        return true;
    }

    /**
     * Override the static::deletable() or this method to control which
     * objects can be deleted.
     */
    public function canDelete(): bool
    {
        if ($this->isNew()) {
            throw new Error('Object is not saved ('.$this->data['values']['id']."), so it can't be deleted", self::OBJECT_NOT_SAVED_ERROR);
        }

        return static::isPrivileged() || 0 < static::deletable()->where('id', '=', $this->data['values']['id'])->count();
    }

    /**
     * Override the static::writable() or this method to which objects can be
     * updated.
     */
    public function canUpdate(): bool
    {
        if ($this->isNew()) {
            throw new Error("Object is not saved, so it can't be updated", self::OBJECT_NOT_SAVED_ERROR);
        }

        return static::isPrivileged() || 0 < static::writable()->where('id', '=', $this->data['values']['id'])->count();
    }

    /**
     * Returns metadata that is needed by the ORM implementation, mainly
     * table name and column information.
     *
     *  @return mixed
     */
    abstract protected static function orm(): array;

    public static function getOpenApiSchema(): Schema
    {
        if (isset(static::$shared[static::class]['openApiSchema'])) {
            return static::$shared[static::class]['openApiSchema'];
        }
        $model = static::modelDefinition();
        $filteredModel = $model;

        $filter = function (array &$root) use (&$filter): void {
            foreach ($root as $k => $v) {
                if (\is_string($k) && '$' === $k[0]) {
                    unset($root[$k]);
                } elseif (\is_array($v)) {
                    $filter($root[$k]);
                }
            }
        };

        $filter($filteredModel);

        foreach ($filteredModel['properties'] as $propertyName => $propertySpec) {
            $filteredModel['properties'][$propertyName] = new Schema($model['$path'].'/properties/'.$propertyName, $propertySpec, [
                'extraProperties' => ['nullable', 'example'],
            ]);
        }

        return static::$shared[static::class]['openApiSchema'] = new Schema($model['$path'], $filteredModel);
    }

    /**
     * Returns metadata that is needed by the ORM implementation, mainly
     * table name and column information.
     *
     *  @psalm-suppress InvalidReturnType
     *
     *  @return ModelSchema
     */
    final public static function modelDefinition(bool $pure = false): array
    {
        if (isset(static::$shared[static::class]['orm'])) {
            return static::$shared[static::class]['orm'];
        }

        $modelDefinition = static::orm();

        // A path reflecting the OpenAPI 3 specification path
        if (isset($modelDefinition['$path'])) {
            $path = $modelDefinition['$path'];
        } else {
            $path = $modelDefinition['$path'] = '/components/schemas/'.str_replace('\\', '-', static::class);
        }

        // The actual schema
        if (!isset($modelDefinition['type'])) {
            $modelDefinition['type'] = 'object';
        }

        // Metadata
        if (!isset($modelDefinition['$meta'])) {
            throw new Error("'$path/\$meta' is not defined", self::MODEL_DEFINITION_ERROR);
        }

        // Property definitions
        if (!isset($modelDefinition['properties'])) {
            throw new Error("'$path/properties' is not defined", self::MODEL_DEFINITION_ERROR);
        }

        // Validate required parts of $modelDefinition['$meta']
        foreach ([
            'plural' => 'Name of the collection',
            'plural_description' => 'Description of the collection',
            'singular' => 'Name of the rows in the collection',
            'singular_description' => 'Description of the rows in the collection',
            'table' => 'Database table name',
            ] as $requiredFieldName => $requiredFieldDescription) {
            if (empty($modelDefinition['$meta'][$requiredFieldName])) {
                throw new Error("$path/\$meta/$requiredFieldName' is undefined ($requiredFieldDescription)", self::VALIDATION_ERROR);
            }
        }

        // Validate illegal fields in $modelDefinition['$meta'] (don't want undocumented extensions)
        $validFields = [
            'plural' => 'Name of the collection',
            'plural_description' => 'Description of the collection',
            'singular' => 'Name of the items in the collection',
            'singular_description' => 'Description of the items in the collection',
            'table' => 'Table name for the model',
            'cols' => 'Array with column names => column definifitions',
            'auto_increment' => 'Boolean value defining if the rows are automatically given "id" column',
            'indexes' => 'Array of index descriptions: array<array{unique?: boolean, 0: string, 1?: string, 2?: string}>',
        ];
        foreach ($modelDefinition['$meta'] as $name => $spec) {
            if (!isset($validFields[$name])) {
                throw new Error("'$path/\$meta/$name' is not a legal key", self::MODEL_DEFINITION_ERROR);
            }
        }

        if (isset($modelDefinition['properties']['id'])) {
            /*
             * The 'id' column is automatically declared
             */
            throw new Error("'$path/properties/id' should not be declared in the model.", self::MODEL_DEFINITION_ERROR);
        }

        /*
         * Inject the primary key column spec
         */
        $modelDefinition['properties'] =
            ['id' => [
                '$extends' => static::TYPE_PRIMARY_KEY,
                'example' => (string) mt_rand(1, 999999999),
            ]] +
            $modelDefinition['properties'];

        /**
         * Function that recursively combines arrays when keys match and both are of type array. Numerical
         * keys are appended, while conflicting keys retain the base value.
         */
        $extend_array = function (array $base, array $parent, string $basePath, string $parentPath) use (&$extend_array): array {
            foreach ($parent as $key => $value) {
                if (\is_int($key)) {
                    $base[] = $key;
                } elseif (!\array_key_exists($key, $base)) {
                    $base[$key] = $value;
                } elseif (\is_array($base[$key]) && \is_array($parent)) {
                    $base[$key] = $extend_array($base[$key], $value, $basePath.'/'.$key, $parentPath.'/'.$key);
                } elseif (\array_key_exists($key, $base)) {
                } else {
                    $base[$key] = $parent[$key];
                }
            }

            return $base;
        };

        /**
         * Validate the properties definition.
         */
        $validFields = [
            '$extends' => 'Extend another type definition',
            //'internal_type' => 'The database type',
            'title' => 'The field name',
            'description' => 'Description of the field',
            'example' => 'An example value',
            'default' => 'The default value or a callable that returns a default value',
            'readOnly' => 'A boolean indicating that the property is read only',
            'writeOnly' => 'A boolean indicating that the property is write only',
            'deprecated' => 'A string describing why the property is deprecated',
            'rules' => 'Validation rules',
            'nullable' => 'A boolean indicating if the field can be null',
        ];
        foreach ($modelDefinition['properties'] as $name => $spec) {
            $spec['$path'] = $modelDefinition['$path'].'/properties/'.$name;
            /**
             * Inherit specifications from the type recursively.
             */
            $parent = $spec['$extends'] ?? null;
            $limit = 512;
            while (null !== $parent) {
                foreach ($parent as $parentKey => $parentValue) {
                    if ('$extends' === $parentKey) {
                        /*
                         * Nested inheritance is resolved in the outer loop
                         */
                        continue;
                    } elseif (!\array_key_exists($parentKey, $spec)) {
                        /*
                         * Simply copy from parent when no conflict
                         */
                        $spec[$parentKey] = $parentValue;
                    } elseif (\is_array($spec[$parentKey]) && \is_array($parentValue)) {
                        /*
                         * All arrays are merged
                         */
                        $spec[$parentKey] = $extend_array($spec[$parentKey], $parentValue, $spec['$path'].'/'.$parentKey, $parent['$path'].'/'.$parentKey);
                    } elseif (null !== $parentValue && \gettype($spec[$parentKey]) !== \gettype($parentValue)) {
                        throw new Error("Type '".\gettype($spec[$parentKey])."' for '".static::class."/properties/$name/$parentKey' is not compatible with inherited type '".\gettype($parentValue)."'", self::MODEL_DEFINITION_ERROR);
                    } else {
                        /*
                         * We don't overwrite the childs values.
                         */
                    }
                }
                $parent = $parent['$extends'] ?? null;

                if (0 === $limit--) {
                    /*
                     * This loop detection is crude, but it is much faster than the alternatives assuming loops
                     * never occur in production.
                     */
                    throw new Error("Loop detected in type inheritance for property '$name' in '".static::class."'", self::MODEL_DEFINITION_ERROR);
                }
            }

            /*
             * After inheritance has been resolved, we don't need the '$type' field any more
             */
            unset($spec['$extends']);
            /*
             * Validate and set defaults for the schema for this column
             */
            if (!\array_key_exists('readOnly', $spec)) {
                $spec['readOnly'] = false;
            }
            if (!\array_key_exists('writeOnly', $spec)) {
                $spec['writeOnly'] = false;
            }
            if (!\array_key_exists('nullable', $spec)) {
                $spec['nullable'] = false;
            }

            /*
             * Check for column properties that are required but not implemented
             */
            if (!empty($spec['$requires'])) {
                foreach ($spec['$requires'] as $requiredKey => $requirementDescription) {
                    if (($spec[$requiredKey] ?? null) === null) {
                        throw new Error("'".$spec['$path']."/$requiredKey' is undeclared (".json_encode($requirementDescription).')', self::MODEL_DEFINITION_ERROR);
                    }
                }
            }

            $modelDefinition['properties'][$name] = $spec;
        }

        $event = self::events()->emit(self::MODEL_DEFINITION_EVENT, (object) ['modelDefinition' => $modelDefinition], false);
        /**
         * @var array
         */
        $modelDefinition = $event->modelDefinition;

        return static::$shared[static::class]['orm'] = $modelDefinition;
    }

    public static function db(): DB
    {
        // @todo Make this configurable, we don't want to depend on Charm\App.
        return app('db');
    }

    /**
     * No need to reveal all metadata
     */
    public function __debugInfo()
    {
        $properties = static::modelDefinition()['properties'];
        $result = [];
        $data = [];
        foreach ($properties as $key => $spec) {
            if (\array_key_exists($key, $this->data['values'])) {
                $result[$key] = $this->$key;
                $data[$key] = $this->data['values'][$key];
            } elseif (\array_key_exists('default', $spec)) {
                if (\is_callable($spec['default'])) {
                    $result[$key] = \call_user_func($spec['default']);
                } else {
                    $result[$key] = $spec['default'].' (DEFAULT)';
                }
            } else {
                $result[$key] = null;
            }
        }

        return $result + [
            '$orm' => [
                'data' => $data,
                'dirty' => $this->data['dirty'],
                'in_database' => $this->data['in_database'],
                ],
            ];
    }

    protected function unsafeSet(string $name, mixed $value): void
    {
        $this->data['values'][$name] = $value;
    }

    protected function unsafeGet(string $name): mixed
    {
        return $this->data['values'][$name] ?? null;
    }

    /**
     * To declare extra computed properties, simply create a method named 'get__propertyName'.
     */
    final public function __get(string $name)
    {
        /*
         * Allow declaring getters by creating a method named 'get__propertyName'
         */
        if (method_exists($this, $cb = 'get__'.$name)) {
            return $this->$cb();
        }

        
        if ('id' !== $name && !$this->isNew()) {
            $ctx = Context::instance();
            if (!empty($ctx->db->transactionTag) && $ctx->db->transactionTag !== $this->data['transaction_tag']) {
                // We are in a transaction, and the transaction tag is wrong
                throw new Error("Accessing object '".static::class.'('.$this->data['values']['id'].")::$name' which was loaded outside of the transaction is a potential error. Reload the object inside the transaction.", self::TRANSACTION_BOUNDARIES_ERROR);
            }
        }

        /**
         * Get model definition.
         */
        $model = static::modelDefinition();

        /*
         * Let the developer know that he's got a typo or bug
         */
        if (!\array_key_exists($name, $model['properties'])) {
            throw new Error("Column '$name' is not declared in model '".static::class."'", self::PROPERTY_NOT_FOUND_ERROR);
        }

        /*
         * Return the default value if we don't have a value
         */
        if (\array_key_exists($name, $this->data['values'])) {
            $result = $this->data['values'][$name];
        } else {
            $result = $model['properties'][$name]['default'] ?? null;
            if (\is_callable($result)) {
                $result = \call_user_func($result);
            }
        }

        /*
         * Allow filtering of the retrieved value
         */
        return $this->getFilter($result, $model['properties'][$name]);
    }

    /**
     * To declare a setter, create a method named 'set__propertyName'.
     */
    final public function __set(string $name, mixed $valueToSet)
    {
        /*
         * Allow declaring setters by creating a method named 'set__propertyName'
         */
        if (method_exists($this, $cb = 'set__'.$name)) {
            return $this->$cb($valueToSet);
        }

        $ctx = Context::instance();
        if (!empty($ctx->db->transactionTag) && $this->data['transaction_tag'] !== $ctx->db->transactionTag) {
            // we must check the transactionTag
            throw new Error("Setting properties on '".static::class.':'.$this->data['values']['id']."' which was loaded outside of the current transaction might cause unintended problems. Use \$object->reload() to reload the object inside the transaction.", self::TRANSACTION_BOUNDARIES_ERROR);
        }

        /**
         * Get model definition.
         */
        $model = static::modelDefinition();

        /*
         * We don't support anonymous properties.
         */
        if (!\array_key_exists($name, $model['properties'])) {
            throw new Error("'".$model['$path']."/properties/$name' is not declared", self::PROPERTY_NOT_FOUND_ERROR);
        }

        /*
         * Only allow writing if the column is not read only
         */
        if (0 === static::$elevatedPrivilegesCounter && !empty($model['properties'][$name]['readOnly'])) {
            throw new Error("'".$model['$path']."/properties/$name' is read only", self::READ_ONLY_PROPERTY_ERROR);
        }

        /**
         * Allow filtering of the value.
         */
        $valueToStore = $this->setFilter($valueToSet, $model['properties'][$name]);

        /*
        if ('modified' === $name) {
            if ('string' === $valueToSet) {
                throw new Exception();
            }
            var_dump($valueToStore, $valueToSet);
            exit('MODIFIED');
        }
        */

        /*
         * Prevent changing the 'id' column if this row came from the database
         */
        if ('id' === $name && $this->data['from_database']) {
            throw new Error("Can't change the 'id' column", self::ILLEGAL_OPERATION_ERROR);
        }

        /*
         * If nothing is changing, we don't have to complete
         */
        if (\array_key_exists($name, $this->data['values']) && $valueToStore === $this->data['values'][$name]) {
            return;
        }

        /**
         * If the object came from the database, we record the original value
         * in $this->data['values_original']. This allows us to retrieve and
         * log any changes whenever we want.
         */
        if (!\array_key_exists($name, $this->data['values_original'])) {
            $this->data['values_original'][$name] = $this->data['values'][$name] ?? null;
        }
        $this->data['values'][$name] = $valueToStore;
        $this->data['dirty'] = true;
    }

    /**
     * Values are stored internally in a format that maps directly to the database. This means we store them as
     * scalar values. This filter takes user input and converts it to a scalar representation that could be passed
     * to a database query or json serialized. If it receives a value that can't be converted, it throws \TypeError
     * exceptions.
     *
     * @param mixed $value
     *
     * @return scalar|null
     */
    private function setFilter($value, array $col)
    {
        if (!isset($col['$meta']['type'])) {
            throw new Error('Requires a resolved colum definition. Did this come directly from ORM::orm()?', self::INCORRECT_USAGE_ERROR);
        }

        /*
         * Setting a value to null is handled here for all types, and also at the end for types
         * that are parsed to null
         */
        if (null === $value) {
            if (!$col['nullable']) {
                throw new TypeError("'".$col['$path']."' is not nullable.", self::VALIDATION_ERROR);
            }

            return $value;
        }

        /*
         * Convert the received value to the type/value the database expects. Validation should not be performed
         * here, except for strictly type checking.
         */
        switch ($col['$meta']['type']) {
            /*
            case self::TYPE_FOREIGN_KEY:
            case self::TYPE_PRIMARY_KEY:
            case self::TYPE_INT:
            */
            case 'int':
                if (\is_string($value) && '' === $value) {
                    $result = null;
                } else {
                    $result = filter_var($value, \FILTER_VALIDATE_INT, \FILTER_NULL_ON_FAILURE);
                    if (null === $result) {
                        throw new TypeError("'".$col['$path']."' expects an integer value", self::VALIDATION_ERROR);
                    }
                }

                break;

            case 'bool':
                if (\is_string($value) && '' === $value) {
                    $result = null;
                } else {
                    $result = (bool) filter_var($value, \FILTER_VALIDATE_BOOL, \FILTER_NULL_ON_FAILURE);
                }

                break;

            case 'float':
                if (\is_string($value) && '' === $value) {
                    $result = null;
                } else {
                    $result = filter_var($value, \FILTER_VALIDATE_FLOAT, \FILTER_NULL_ON_FAILURE);
                    if (null === $result) {
                        throw new TypeError("'".$col['$path']."' expects a float value", self::VALIDATION_ERROR);
                    }
                }
                break;

            case 'datetime':
                if (\is_object($value)) {
                    if (is_a($value, DateTimeImmutable::class)) {
                        $result = $value
                            ->setTimezone(new DateTimeZone(date_default_timezone_get()))
                            ->format('Y-m-d H:i:s');
                    } elseif ($value instanceof DateTimeInterface) {
                        $result = DateTimeImmutable::createFromInterface($value)
                            ->setTimezone(new DateTimeZone(date_default_timezone_get()))
                            ->format('Y-m-d H:i:s');
                    } else {
                        throw new TypeError("'".$col['$path']."' expects an object that implements DateTimeInterface, a UNIX timestamp or a string parseable to a datetime object", self::VALIDATION_ERROR);
                    }
                } elseif (\is_string($value)) {
                    if ('' === $value) {
                        $result = null;
                    } else {
                        $ts = strtotime($value);
                        if (false === $ts) {
                            throw new TypeError("'".$col['$path']."' expects a datetime object or a string that is parseable according to common time string formats.", self::VALIDATION_ERROR);
                        }

                        $result = DateTimeImmutable::createFromFormat('U', (string) $ts);
                        if ($result) {
                            $result = $result->format('Y-m-d H:i:s');
                        }
                    }
                } elseif (is_numeric($value)) {
                    $result = DateTimeImmutable::createFromFormat('U.u', number_format((float) $value, 6));
                    if ($result) {
                        $result = $result->format('Y-m-d H:i:s');
                    }
                } else {
                    throw new TypeError("'".$col['$path']."' expects a datetime object or a string that is parseable according to common time string formats.", self::VALIDATION_ERROR);
                }
                /*
                if (null !== $result) {
                    var_dump($result);die();
                    $result = $result
                        ->setTimezone(new DateTimeZone(date_default_timezone_get()))
                        ->format('Y-m-d H:i:s');
                }
                */
                break;

            case 'json':
                $result = json_encode($value);
                break;

            case 'enum':
                // TODO 'enum' probably is not a type, it is just a set of legal values...
                $result = $value;
                break;

            case 'string':
                $result = (string) $value;
                break;

            case 'decimal':
                /*
                 * Decimal values are stored as strings internally
                 */
                if (\is_float($value)) {
                    $result = number_format($value, $col['$scale'], '.', '');
                /*
                 * @todo Decide how strict to be on this... or make it configurable. Floats are risky in this scenario.
                 */
                } else {
                    $result = (string) $value;
                }

                break;

            default:
                /*
                 * Allow extending the type system
                 */
                $result = self::hooks()->filter(__METHOD__, $value, $col);
                break;
        }

        /**
         * Psalm is mixing up. The value CAN be null.
         *
         * @var mixed
         */
        $value = $value;

        /*
         * If the value was filtered to null, then check if that is okay.
         */
        if (null === $value) {
            if (!$col['nullable']) {
                throw new Error("'".$col['$path']."' is not nullable.");
            }

            return $value;
        }

        if (!is_scalar($result)) {
            throw new Error("Filtered value for '".$col['$path']."' with internal type '".$col['internal_type']."' is not a scalar type. Error in filtering.", self::INTERNAL_ERROR);
        }

        return $result;
    }

    /**
     * Set a value directly in the database, without requiring locking of the row and without making
     * the object "dirty".
     *
     * @param mixed $value
     *
     * @return void
     */
    public function directSet(string $key, $value)
    {
        if ($this->isNew()) {
            throw new Error("Can't use ORM::directSet() on objects that have not been saved.", self::INCORRECT_USAGE_ERROR);
        }

        if (\array_key_exists($key, $this->data['values_original'])) {
            throw new Error('Attempting to use ORM::directSet() on a property that was already dirty', self::INCORRECT_USAGE_ERROR);
        }
        $previousData = $this->data;
        $this->$key = $value;
        if ($previousData['values'][$key] === $this->data['values'][$key]) {
            return;
        }

        $errors = $this->isInvalid();
        if (!empty($errors[$key])) {
            throw new Error($key.': '.$errors[$key], self::VALIDATION_ERROR);
        }

        $model = static::modelDefinition();

        $db = $this->db();
        $result = $db->exec('UPDATE '.$model['$meta']['table'].' SET '.$db->quoteIdentifier($key).'=? WHERE id=?', [$this->data['values'][$key], $this->data['values']['id']]);
        if (!$result) {
            throw new Error("Could not update property '$key' using ORM::directSet()", self::DATABASE_RELATED_ERROR);
        }
        unset($this->data['values_original'][$key]);
        $this->data['dirty'] = $previousData['dirty'];
    }

    /**
     * Values are stored internally as scalar values in a format suitable for storage in
     * databases. This filter converts those values to the representation that developers
     * receive when they access the value as a property $model->propertyName.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    private function getFilter($value, array $col)
    {
        /*
        * Convert the type FROM the internal/PDO mapped type
        */
        switch ($col['$meta']['type']) {
            case 'bool':
                $result = (bool) $value;
                break;
            case 'int':
                $result = (int) $value;
                break;
            case 'float':
                $result = (float) $value;
                break;
            case 'decimal':
                $result = $value;
                break;
            case 'datetime':
                if (null !== $value) {
                    $result = new ORM\Util\DateTimeImmutable($value);
                } else {
                    $result = null;
                }
                break;
            case 'enum':
                $result = $value;
                break;
            case 'json':
                if (\is_string($value)) {
                    $result = json_decode($value);
                } else {
                    $result = null;
                }
                break;
            case 'string':
                $result = $value;
                break;
            default:
                throw new Error("'".$col['$path']."/\$meta/type': Unknown type '".$col['$meta']['type']."'", self::INTERNAL_ERROR);
                $result = $value;
                break;
        }

        return self::hooks()->filter(__METHOD__, $result, $col, $value);
    }

    /**
     * Add an error to the validator results. Mostly used for
     * fields that don't exist, such as "confirm_email" etc, but
     * can also be used to add errors to existing fields.
     *
     * When adding an error to a field that does not exist, you
     * may specify a caption which can be used in rendering the
     * error message through `$this->getError()`.
     */
    public function addError(string $fieldName, string $message, string $addedCaption = null): void
    {
        $this->addedErrors[$fieldName] = [$message, $addedCaption];
    }

    /**
     * @var array<string, array{0: string, 1: null|string}>
     */
    private $addedErrors = [];

    /**
     * Returns true if the field is registered with an error. Allows
     * fields that don't exist, to allow for special fields like
     * email confirmation or terms acceptation fields.
     */
    public function hasError(string $fieldName): bool
    {
        $errors = $this->isInvalid();

        if (!\is_array($errors)) {
            return false;
        }

        return !empty($_POST) && !empty($errors[$fieldName]);
    }

    /**
     * Did this object come from a database?
     */
    final public function isNew(): bool
    {
        return !$this->data['in_database'];
    }

    /**
     * Has this object been changed and not been saved?
     */
    final public function isDirty(): bool
    {
        return $this->data['dirty'];
    }

    final public function getChangedColumns(): array
    {
        return $this->data['values_original'];
    }

    /**
     * Validate the columns before they are saved to the database. Return an empty
     * array if there are no errors, and an associative array of column names to
     * error message if there are errors.
     */
    public function isInvalid(): mixed
    {
        return $this->internalIsInvalid();
    }

    /**
     * Perform validation using only the model definition from static::orm() and any
     * added errors.
     *
     * @param array $onlyTheseColumns
     */
    private function internalIsInvalid(array $onlyTheseColumns = null): mixed
    {
        $db = null;
        $errors = [];
        foreach ($this->addedErrors as $fieldName => $def) {
            if (null !== $onlyTheseColumns && \in_array($fieldName, $onlyTheseColumns)) {
                continue;
            }
            $errors[$fieldName] = $def[0];
        }
        $model = static::modelDefinition();

        /*
         * Prepare values for validation, set defaults
         */
        foreach ($model['properties'] as $name => $col) {
            if (null !== $onlyTheseColumns && \in_array($name, $onlyTheseColumns)) {
                continue;
            }

            if (isset($errors[$name])) {
                continue;
            }

            /**
             * @var DB
             */
            $db = null;

            /*
             * Set default values
             */
            if (!\array_key_exists($name, $this->data['values']) && \array_key_exists('default', $col)) {
                $default = $col['default'];
                if (\is_callable($default)) {
                    $default = \call_user_func($default);
                }
                $this->data['values'][$name] = $this->setFilter($default, $col);
                $this->data['values_original'][$name] = null;
            }

            /*
             * Special handling of 'id' key
             */
            if ('id' === $name) {
                if ($this->data['in_database']) {
                    // This row is going to be updated
                    if (!isset($this->data['values']['id']) || null === $this->data['values']['id']) {
                        $errors[$name] = "'".$col['$path']."' is NULL for a record that came from database";
                        continue;
                    }
                } else {
                    // This is a new row
                    if (isset($this->data['values']['id']) && null !== $this->data['values']['id']) {
                        $errors[$name] = "'".$col['$path']."' must be NULL when saving new objects";
                        continue;
                    }
                }
                continue;
            }
        }

        $validator = new OpenApiValidator($model);
        $validatorErrors = $validator->isInvalid($this->data['values']);
        if (null !== $validatorErrors) {
            if (\is_string($validatorErrors)) {
                return $validatorErrors;
            }
            $errors += $validatorErrors;
        }

        if (null !== $onlyTheseColumns) {
            foreach ($errors as $fieldName => $null) {
                if (!\in_array($fieldName, $onlyTheseColumns)) {
                    unset($errors[$fieldName]);
                }
            }
        }

        /*
         * Additional special validation
         */
        foreach ($model['properties'] as $name => $col) {
            if (null !== $onlyTheseColumns && \in_array($name, $onlyTheseColumns)) {
                continue;
            }

            if (isset($errors[$name])) {
                continue;
            }

            if ('id' === $name) {
                continue;
            }

            $value = $this->data['values'][$name] ?? null;

            /*
                        if (!empty($col['$meta']['unique'])) {
                            $db = $db ?? DB::instance();

                            if (true === $col['$meta']['unique']) {
                                // simple uniqueness check
                                if ($this->isNew()) {
                                    if ($db->query('SELECT 1 FROM '.$model['$meta']['table'].' WHERE '.$db->quoteIdentifier($name).'=? LIMIT 1', [$value], 0)->current()) {
                                        $errors[$name] = 'Already exists';
                                        continue;
                                    }
                                } else {
                                    if ($db->query('SELECT 1 FROM '.$model['$meta']['table'].' WHERE id<>? AND '.$db->quoteIdentifier($name).'=? LIMIT 1', [$this->unsafeGet('id'), $value], 0)->current()) {
                                        $errors[$name] = 'Already exists';
                                        continue;
                                    }
                                }
                            }
                        }
            */
            /*
             * If this is a foreign key, check that the target object exists
             */
            if (null !== $value && !empty($col['$model'])) {
                $fkClass = $col['$model'];
                $fkInstance = $fkClass::loadUnsafe($value);
                if (!$fkInstance) {
                    $errors[$name] = 'Not found';
                    continue;
                }
            }
        }

        if (!empty($model['$meta']['indexes'])) {
            foreach ($model['$meta']['indexes'] as $indexDef) {
                if (!empty($indexDef['unique'])) {
                    // The fields must be unique
                    $existing = static::allUnsafe();
                    $useQuery = false;
                    foreach ($indexDef as $k => $field) {
                        if (null !== $onlyTheseColumns && \in_array($field, $onlyTheseColumns)) {
                            continue;
                        }

                        if (!\array_key_exists($field, $this->data['values_original'])) {
                            continue;
                        }

                        if (\array_key_exists($field, $errors)) {
                            $useQuery = false;
                            continue;
                        }

                        if (\is_int($k)) {
                            $useQuery = true;
                            $existing = $existing->where($field, '=', $this->data['values'][$field] ?? null);
                        } else {
                            unset($indexDef[$k]);
                        }
                    }

                    if ($useQuery) {
                        foreach ($existing->limit(2) as $existingRow) {
                            if ($existingRow->data['values']['id'] === ($this->data['values']['id'] ?? null)) {
                                continue;
                            }
                            $indexedCols = [];
                            foreach ($indexDef as $k => $field) {
                                if (\is_int($k)) {
                                    $indexedCols[] = $field;
                                }
                            }
                            foreach ($indexDef as $k => $field) {
                                if (\is_int($k)) {
                                    if (!isset($errors[$field])) {
                                        if (\count($indexDef) > 1) {
                                            $errors[$field] = "Combination of fields '".implode("' and '", $indexedCols)."' already exists";
                                        } else {
                                            $errors[$field] = 'Already exists';
                                        }
                                    }
                                }
                            }
                            // Won't search for further errors once we have these
                            break 2;
                        }
                    }
                }
            }
        }

        if (0 === \count($errors)) {
            return null;
        }

        return $errors;
    }

    public static function allUnsafe(): TableInterface
    {
        $model = static::modelDefinition();

        return new ORM\Table(static::class, $model['$meta']['table']);
    }

    /**
     * Returns all the database rows that are readable by the current user. Override
     * this to restrict read/write/delete access. Further restrictions can be added
     * my overriding the static::writable() and static::deletable() methods.
     */
    public static function all(): TableInterface
    {
        return static::allUnsafe();
    }

    /**
     * Returns all the database rows that are writable. Override this to limit
     * write access per row.
     */
    public static function writable(): TableInterface
    {
        return static::all();
    }

    /**
     * Returns all the database rows that are deletable.
     */
    public static function deletable(): TableInterface
    {
        return static::all();
    }

    public function delete(): bool
    {
        if (!$this->canDelete()) {
            throw new Error('Access Denied deleting object', self::PERMISSION_DENIED_ERROR);
        }

        $result = $this->deleteUnsafe();

        return $result;
    }

    public function deleteUnsafe(): bool
    {
        // check if row is in the database at all
        if ($this->isNew()) {
            throw new Error('Deleting an object that is not in the database makes no sense.');
        }

        $model = static::modelDefinition();

        $event = (object) [
            'target' => $this,
            'dirty' => $this->data['dirty'],
            'from_database' => $this->data['from_database'] ?? null,
            'in_database' => $this->data['in_database'] ?? null,
        ];

        if (self::events()->emit(self::DELETE_EVENT, $event, true)->cancelled) {
            throw new CancelledException('Deletion was cancelled by event listener');
        }

        $result = $this->db()->exec('DELETE FROM '.$model['$meta']['table'].' WHERE id=?', [$this->id]);
        if ($result) {
            $this->data['values_original'] += $this->data['values'];
            $this->data['values']['id'] = null;
            $this->markDeleted();
            self::events()->emit(self::DELETED_EVENT, $event, false);

            return true;
        } else {
            return false;
        }
    }

    public function save(array $onlyTheseColumns = null): bool
    {
        if ($this->isNew() && !static::canCreate()) {
            throw new Error("Access denied creating '".static::class."' objects", self::PERMISSION_DENIED_ERROR);
        } elseif (!$this->isNew() && !$this->canUpdate()) {
            throw new Error("Don't have rights to update this object", self::PERMISSION_DENIED_ERROR);
        }
        if (null !== ($errors = $this->isInvalid())) {
            if (\is_string($errors)) {
                throw new ValidationError($errors, []);
            }
            if (null !== $onlyTheseColumns) {
                foreach ($errors as $k => $v) {
                    if (!\in_array($k, $onlyTheseColumns)) {
                        unset($errors[$k]);
                    }
                }
            }
            if (\count($errors) > 0) {
                throw new ValidationError('Validation Errors', $errors);
            }
        }
        $db = $this->db();
        $cols = [];
        $colsUpdate = [];
        $vals = [];
        $model = static::modelDefinition();

        /*
         * Build arrays for the SQL query
         */
        foreach ($model['properties'] as $name => $info) {
            if ('id' === $name) {
                // treat the 'id' column differently
                continue;
            }
            if (null !== $onlyTheseColumns) {
                if (!\in_array($name, $onlyTheseColumns)) {
                    // If this column is not being saved, skip
                    continue;
                }
            }
            $cols[] = $db->quoteIdentifier($name);
            $colsUpdate[] = $db->quoteIdentifier($name).'=?';
            $vals[] = $this->data['values'][$name] ?? null; // $this->data['cols'][$name] ?? null;
        }

        $event = (object) [
            'target' => $this,
            'columns' => $onlyTheseColumns,
            'dirty' => $this->data['dirty'],
            'from_database' => $this->data['from_database'] ?? null,
            'in_database' => $this->data['in_database'] ?? null,
            'values' => $this->data['values'],
            'values_original' => $this->data['values_original'],
        ];

        self::events()->emit(self::SAVE_EVENT, $event, false);

        if ($this->data['in_database']) {
            /*
             * This object is in the database
             */

            $vals[] = $this->data['values']['id'];
            $sql = 'UPDATE '.$model['$meta']['table'].' SET '.implode(',', $colsUpdate).' WHERE id=? LIMIT 1';

            $result = $db->exec($sql, $vals);
            if (false !== $result) {
                $this->markSaved($onlyTheseColumns);
                self::events()->emit(self::SAVED_EVENT, $event, false);

                return true;
            } else {
                throw new ORM\Error("Could not save (sql=$sql)");
            }
        } else {
            // Guard from saving an incomplete object
            if (null !== $onlyTheseColumns) {
                throw new ORM\Error('Must save all columns for new objects');
            }
            $id = null;
            if (false === ($model['$meta']['auto_increment'] ?? false)) {
                $id = ORM\IdGenerator::get()->id();
                array_unshift($cols, $db->quoteIdentifier('id'));
                array_unshift($vals, $id);
            }

            $sql = 'INSERT INTO '.$model['$meta']['table'].' ('.implode(',', $cols).') VALUES ('.implode(',', array_fill(0, \count($cols), '?')).')';
            try {
                $result = $db->exec($sql, $vals);
            } catch (DBError $e) {
                throw $e;
            }

            if ($result) {
                if (null === $id) {
                    $this->data['values']['id'] = $db->lastInsertId();
                } else {
                    $this->data['values']['id'] = $id;
                }
                $this->markSaved($onlyTheseColumns);
                self::events()->emit(self::SAVED_EVENT, $event, false);

                return true;
            } else {
                throw new ORM\Error("Could not save (sql=$sql)");
            }
        }
    }

    /**
     * Bypass access restrictions.
     *
     * @return static|null
     */
    final public static function loadUnsafe(mixed $id)
    {
        $q = static::allUnsafe()->where('id', '=', $id)->one();

        return $q;
    }

    /**
     * Override the static::all() method to control which rows are
     * readable.
     *
     * @return static|null
     */
    final public static function load(mixed $id)
    {
        $q = static::all()->where('id', '=', $id)->one();

        return $q;
    }

    /**
     * Load this object from the database
     *
     * @return static|null
     */
    final public function reload()
    {
        if ($this->isNew()) {
            throw new Error("Can't reload an unsaved object");
        }
        /*
         * No need to use safe load because we already have the object.
         */
        return static::loadUnsafe($this->data['values']['id']);
    }

    /**
     * Override this method to control what data is returned in
     * API queries.
     */
    public function jsonSerialize()
    {
        $model = static::modelDefinition();

        $json = [];
        foreach ($model['properties'] as $name => $col) {
            if ($col['writeOnly'] ?? null) {
                continue;
            }
            $json[$name] = $this->$name;
        }

        return $json;
    }

    /**
     * Method is used when creating object instances from the database.
     *
     * @param array<string, mixed> $columns
     * @param class-string         $className
     *
     * @return static
     */
    public static function createFromColumnsArray(array $columns, string $className = null)
    {
        if (null === $className) {
            $className = static::class;
        }
        if (!class_exists($className)) {
            throw new Error("Class '$className' does not exist.");
        }
        $ref = new ReflectionClass($className);

        return static::elevatePrivileges(function () use ($columns, $ref) {
            $instance = $ref->newInstanceWithoutConstructor();
            $instance->data['values'] = $columns;
            $instance->markFromDatabase();

            return $instance;
        });
    }

    public function getObjectReference(): ObjectReference
    {
        return new ObjectReference($this, [$this->id]);
    }

    /**
     * @return ?static
     */
    public static function loadFromReference(array $keys)
    {
        return static::loadUnsafe($keys[0]);
    }

    private static function hooks(): Hooks
    {
        return Hooks::instance();
    }

    private function set__transactionTag(?string $value) {
        $this->data['transaction_tag'] = $value;
    }

    final protected function get__transactionTag(): ?string {
        return $this->data['transaction_tag'] ?? null;
    }
}
