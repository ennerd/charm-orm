<?php
declare(strict_types=1);

namespace Charm\ORM;

class Error extends \Charm\Error {

    protected $httpStatus = "Internal ORM Error";

}