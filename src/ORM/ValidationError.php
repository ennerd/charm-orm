<?php
declare(strict_types=1);

namespace Charm\ORM;

class ValidationError extends \Charm\Error {

    public function __construct(string $message, array $validationErrors=[]) {
        parent::__construct($message, 400, null, [
            400, 'Bad Request',
            'errors' => $validationErrors,
        ]);
    }

    public function jsonSerialize() {
        $result = [
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
            'properties' => $this->extra['errors'],
        ] + parent::jsonSerialize();
        //unset($result['DEBUG_IS_ENABLED']);
        return $result;
    }

}