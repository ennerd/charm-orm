<?php
declare(strict_types=1);

namespace Charm\ORM;

use Godruoyi\Snowflake\Snowflake;
use Godruoyi\Snowflake\Sonyflake;

/**
 * Generates unique primary keys using the Snowflake algorithm with help from Godruoyi.
 *
 * Usage:
 *  ```
 *  $id = \Charm\Orm\IdGenerator::get()->id();
 *  ```
 */
class IdGenerator
{
    const SONYFLAKE = Sonyflake::class;
    const SNOWFLAKE = Snowflake::class;

    public static $type = self::SONYFLAKE;
    public static $datacenterId = null;
    public static $workerId = null;
    public static $startTime = 1577833200; // 2015-01-01 00:00:00

    protected static $instances = [];

    public static function get(int $workerId = null, int $datacenterId = null): Snowflake
    {
        if (null === $datacenterId) {
            $datacenterId = static::$datacenterId;
        }
        if (null === $workerId) {
            $workerId = static::$workerId;
        }
        if (isset(static::$instances[$datacenterId ?? -1][$workerId ?? -1])) {
            return static::$instances[$datacenterId ?? -1][$workerId ?? -1];
        }

        $className = static::$type;

        switch ($className) {
            case static::SNOWFLAKE:
                $instance = new $className($workerId, $datacenterId);
                break;
            case static::SONYFLAKE:
                $instance = new $className(null !== $workerId ? $workerId : 0);
                break;
            default:
                throw new Error(static::class.'::$type must be either static::SNOWFLAKE or static::SONYFLAKE');
        }

        $instance->setStartTimeStamp(static::$startTime);

        return static::$instances[$datacenterId ?? -1][$workerId ?? -1] = $instance;
    }
}
