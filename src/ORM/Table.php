<?php
declare(strict_types=1);

namespace Charm\ORM;

use Charm\DB;
use Charm\Table\AbstractTable;

class Table extends AbstractTable
{
    protected $className;
    protected string $table;
    protected array $wheres = [];
    protected ?string $order = null;
    protected bool $orderDesc = false;
    protected DB $databaseConnection;

    /**
     * @var array<array{0: string, 1: array<mixed>}>
     */
    protected array $restrictions = [];

    public function __construct(string $className, string $table)
    {
        $this->className = $className;
        $this->table = $table;
    }

    protected function hasColumn(string $name) {
        $modelDefinition = $this->className::modelDefinition();
        return isset($modelDefinition['properties'][$name]);
    }

    /**
     * Restrict the data source by declaring an SQL query. Consider applying restrictions
     * using DB::getViews instead...
     */
    public function restrict(string $sql, array $vars = []): self
    {
        $clone = clone $this;
        $clone->restrictions[] = [$sql, $vars];

        return $clone;
    }

    /**
     * Returns the where component from the database query.
     *
     * @return null|array{0: list<string>, 1: mixed}
     */
    final public function getWheres(): ?array
    {
        if (0 === \count($this->wheres)) {
            return null;
        }
        /**
         * @var list<mixed>
         */
        $vars = [];
        $wheres = array_values(array_map(function ($filter) use (&$vars) {
            switch ($filter[1]) {
                case 'lt':
                    $vars[] = $filter[2];
                    return $filter[0].' < ?';
                case 'lte':
                    $vars[] = $filter[2];
                    return $filter[0].' <= ?';
                case 'gt':
                    $vars[] = $filter[2];
                    return $filter[0].' > ?';
                case 'gte':
                    $vars[] = $filter[2];
                    return $filter[0].' >= ?';
                case 'eq':
                    if ($filter[2] === null) {
                        return $filter[0].' IS NULL';
                    }
                    $vars[] = $filter[2];
                    return $filter[0].' = ?';
                default:
                    throw new Error("Unknown filter type '".$filter[1]."'");
            }
        }, $this->wheres));

        return [0 => $wheres, 1 => $vars];
    }

    public function getIterator()
    {
        $vars = [];
        $source = $this->table;

        foreach ($this->restrictions as $restriction) {
            if (null !== $restriction[0]) {
                $source = '(SELECT * FROM '.$source.' WHERE '.$restriction[0].') AS '.$this->table;
                foreach ($restriction[1] as $var) {
                    $vars[] = $var;
                }
            } else {
                $source = '(SELECT * FROM '.$source.') AS '.$this->table;
            }
        }

        $sql = 'SELECT * FROM '.$source;
        if (null !== ($wheres = $this->getWheres())) {
            $sql .= ' WHERE ';
            $sql .= implode(' AND ', $wheres[0]);
            foreach ($wheres[1] as $var) {
                $vars[] = $var;
            }
        }
        if (null !== $this->order) {
            $sql .= ' ORDER BY '.$this->order;
            if ($this->orderDesc) {
                $sql .= ' DESC';
            }
        }
        $sql .= ' LIMIT '.(int) ($this->offset).','.(int) ($this->limit);
        $className = $this->className;
        foreach (DB::instance()->query($sql, $vars, 'array') as $row) {
            $row = $className::createFromColumnsArray($row);
            $row->transactionTag = DB::instance()->transactionTag;
            yield $row;
        }
    }

    public function count(): int
    {
        $sql = 'SELECT COUNT(id) FROM '.$this->table;

        if (null !== ($wheres = $this->getWheres())) {
            $sql .= ' WHERE '.implode(' AND ', $wheres[0]);
            $vars = $wheres[1];
        } else {
            $vars = [];
        }
        $sql .= ' LIMIT 1';
        foreach (DB::instance()->query($sql, $vars, 0) as $res) {
            return (int) $res;
        }
        throw new Error('No result from COUNT');
    }

    public function offset(int $offset): self
    {
        $clone = clone $this;
        $clone->offset = max(0, $offset);

        return $clone;
    }

    public function limit(int $limit): self
    {
        $clone = clone $this;
        $clone->limit = min(1000, $limit);

        return $clone;
    }

    public function lt(string $column, $value): self
    {
        if (!$this->hasColumn($column)) {
            throw new Error("Unknown column '$column' in '".$this->table."'");
        }
        $clone = clone $this;
        $clone->wheres[] = [$column, 'lt', $value];

        return $clone;
    }

    public function lte(string $column, $value): self
    {
        if (!$this->hasColumn($column)) {
            throw new Error("Unknown column '$column' in '".$this->table."'");
        }
        $clone = clone $this;
        $clone->wheres[] = [$column, 'lte', $value];

        return $clone;
    }

    public function gt(string $column, $value): self
    {
        if (!$this->hasColumn($column)) {
            throw new Error("Unknown column '$column' in '".$this->table."'");
        }
        $clone = clone $this;
        $clone->wheres[] = [$column, 'gt', $value];

        return $clone;
    }

    public function gte(string $column, $value): self
    {
        if (!$this->hasColumn($column)) {
            throw new Error("Unknown column '$column' in '".$this->table."'");
        }
        $clone = clone $this;
        $clone->wheres[] = [$column, 'gte', $value];

        return $clone;
    }

    public function eq(string $column, $value): self
    {
        if (!$this->hasColumn($column)) {
            throw new Error("Unknown column '$column' in '".$this->table."'");
        }

        $clone = clone $this;
        $clone->wheres[] = [$column, 'eq', $value];

        return $clone;
    }
}
