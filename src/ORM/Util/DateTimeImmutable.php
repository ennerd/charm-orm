<?php
declare(strict_types=1);

namespace Charm\ORM\Util;

use DateTimeImmutable as GlobalDateTimeImmutable;
use JsonSerializable;

class DateTimeImmutable extends GlobalDateTimeImmutable implements JsonSerializable
{
    public function jsonSerialize(): string
    {
        return $this->format('Y-m-d\TH:i:s.uP');
    }

    public function __toString(): string
    {
        return $this->jsonSerialize();
    }
}
