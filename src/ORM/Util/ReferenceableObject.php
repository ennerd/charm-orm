<?php
namespace Charm\ORM\Util;

/**
 * Allows object references to be serialized, without serializing all
 * the data.
 */
interface ReferenceableObject {

    /**
     * Returns a string which can be used to reload the object instance from a
     * backend data store. This string must contain
     *
     * @return string
     */
    public function getObjectReference(): ObjectReference;
    public static function loadFromReference(array $keys);
}