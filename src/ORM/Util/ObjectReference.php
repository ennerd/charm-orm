<?php
namespace Charm\ORM\Util;

use Charm\Error;
use Serializable;
use ReflectionClass;
use JsonSerializable;
use Charm\InvalidArgumentError;

final class ObjectReference implements Serializable, JsonSerializable {

    private string $c;
    private array $k;

    public function __construct(ReferenceableObject $object, array $keys) {
        if (!defined('GLOBAL_SECRET')) {
            throw new Error("Constant 'GLOBAL_SECRET' must be defined to use this functionality.");
        }

        foreach ($keys as $key) {
            if (!is_scalar($key)) {
                throw new InvalidArgumentError("Expecting scalar keys");
            }
        }
        $this->c = str_replace("\\", ".", $object::class);
        $this->k = $keys;
    }

    public function load() {
        $className = str_replace(".", "\\", $this->c);
        return $className::loadFromReference($this->k);
    }

    public static function __set_state(array $state) {
        $instance = (new ReflectionClass(self::class))->newInstanceWithoutConstructor();
        $instance->c = $state['c'];
        $instance->k = $state['k'];
        return $instance;
    }

    public function unserialize(string $referenceString) {
        $vars = self::decodeString($referenceString);
        $this->c = $vars['c'];
        $this->k = $vars['k'];
    }

    public function serialize() {
        return $this->encodeString();
    }

    public static function fromString(string $referenceString): self {
        $decoded = self::decodeString($referenceString);
        return self::__set_state($decoded);
    }

    public function toString(): string {
        return $this->encodeString($this);
    }

    public static function fromJsonReference(object $ref): self {
        if (!isset($ref->{'$objRef'})) {
            throw new InvalidArgumentError("Expected a '\$objRef\' property.");
        }
        return self::__set_state(self::decodeArray($ref->{'$objRef'}));
    }

    public function jsonSerialize() {
        return ['$objRef' => $this->encodeArray()];
    }

    public function __toString(): string {
        return $this->encodeString($this);
    }

    /**
     * Encode a string that holds the className and keys with a signature
     *
     * @return string
     */
    private function encodeString(): string {
        return substr(json_encode($this->encodeArray()), 1, -1);
    }

    /**
     * Creates an array<c: className, k: keys> from a string created with encodeString()
     *
     * @param string $reference
     * @return array
     */
    private static function decodeString(string $reference): array {
        if (!defined('GLOBAL_SECRET')) {
            throw new Error("Constant 'GLOBAL_SECRET' must be defined to use this functionality.");
        }

        $components = json_decode('['.$reference.']');
        return self::decodeArray($components);
    }

    /**
     * Creates an array{0: className, 1...n-1: key, n: hash}
     *
     * @return array
     */
    private function encodeArray(): array {
        $components = [$this->c, ...$this->k];
        $hash = rtrim(base64_encode(hash_hmac('md5', implode(",", $components), \GLOBAL_SECRET, true)), "=");
        $components[] = $hash;
        return $components;        
    }

    /**
     * Creates an array<c: className, k: keys> from an array{0: className, 1...n-1: key, n: hash}
     *
     * @param array $array
     * @return array
     */
    private static function decodeArray(array $array): array {
        $hash = array_pop($array);
        if (!is_string($hash)) {
            throw new InvalidArgumentError('Array has no signature');
        }
        if ($hash !== rtrim(base64_encode(hash_hmac('md5', implode(",", $array), \GLOBAL_SECRET, true)), "=")) {
            if (defined('EXPIRED_SECRETS')) {
                $date = gmdate('Y-m-d H:i:s');
                foreach (\EXPIRED_SECRETS as $secret => $expiration) {
                    if ($expiration < $date) {
                        continue;
                    }
                    if ($hash === rtrim(base64_encode(hash_hmac('md5', implode(",", $array), $secret, true)), "=")) {
                        goto success;
                    }
                }
            }
            throw new InvalidArgumentError('Signature does not match or is expired');
        }
    success:
        return ['c' => array_shift($array), 'k' => $array];
    }
}