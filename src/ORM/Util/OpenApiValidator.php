<?php
declare(strict_types=1);

namespace Charm\ORM\Util;

use DateTime;
use DateTimeInterface;

class OpenApiValidator
{
    protected array $schema;

    /**
     * @param array|object $schema
     */
    public function __construct(mixed $schema)
    {
        $this->schema = (array) $schema;
    }

    public function isInvalid(mixed $value): mixed
    {
        if (isset($this->schema['enum'])) {
            if (\in_array($value, $this->schema['enum'])) {
                return null;
            } else {
                return 'Illegal value';
            }
        }

        if (null === $value) {
            if (!($this->schema['nullable'] ?? null)) {
                return 'Value is required';
            }
        }

        if (isset($this->schema['type'])) {
            switch ($this->schema['type']) {
                case 'string':
                    if (!\is_string($value)) {
                        return 'Expected a string value';
                    }
                    break;
                case 'integer':
                    if (!is_numeric($value) || (int) $value != $value) {
                        return 'Expected an integer value';
                    }
                    break;
                case 'number':
                    if (!is_numeric($value)) {
                        return 'Expected a number';
                    }
                    break;
                case 'boolean':
                    $filteredValue = filter_var($value, \FILTER_VALIDATE_BOOLEAN, \FILTER_NULL_ON_FAILURE);
                    if (null === $filteredValue) {
                        return 'Expected a boolean value';
                        break;
                    }
                    break;
                case 'array':
                    if ('json' === ($this->schema['format'] ?? null)) {
                        if (!\is_string($value)) {
                            return 'Expected an array JSON';
                        }
                        $value = json_decode($value);
                    }
                    if (!\is_array($value)) {
                        if (\is_string($value)) {
                            return "Expected an array but got string. Format 'json'?";
                        }

                        return 'Expected an array';
                    }
                    if (array_keys($value) !== range(0, \count($value) - 1)) {
                        return 'Expected an array with numeric keys';
                    }
                    break;
                case 'object':
                    if ('json' === ($this->schema['format'] ?? null)) {
                        if (!\is_string($value)) {
                            return 'Expected an object JSON';
                        }
                        $value = json_decode($value);
                    }
                    if (\is_array($value)) {
                        if (array_keys($value) === range(0, \count($value) - 1)) {
                            return 'Expected an object, but received an array with numeric keys';
                        }
                        // type OK
                    } elseif (\is_object($value)) {
                        // type OK
                    } elseif (\is_string($value)) {
                        return "Expected an object but got a string. Format 'json'?";
                    } else {
                        return 'Expected an object';
                    }
                    break;
            }
        }

        if (null !== ($this->schema['minimum'] ?? null)) {
            if ((float) $value < (float) ($this->schema['minimum'])) {
                return 'Minimum value is '.$this->schema['minimum'];
            }
        }
        if ($this->schema['exclusiveMinimum'] ?? null) {
            if (null === ($this->schema['minimum'] ?? null)) {
                if ((float) $value === (float) ($this->schema['minimum'])) {
                    return 'Value must be greater then '.$this->schema['minimum'];
                }
            }
        }

        if (null !== ($this->schema['maximum'] ?? null)) {
            if ((float) $value > (float) ($this->schema['maximum'])) {
                return 'Maximum value is '.$this->schema['maximum'].' got '.$value;
            }
        }
        if ($this->schema['exclusiveMaximum'] ?? null) {
            if (null === ($this->schema['maximum'] ?? null)) {
                if ((float) $value === (float) ($this->schema['maximum'])) {
                    return 'Value must be less then '.$this->schema['maximum'];
                }
            }
        }

        if ((null !== ($this->schema['minLength'] ?? null)) && !\is_object($value) && !\is_array($value)) {
            if (mb_strlen((string) $value) < $this->schema['minLength']) {
                return 'Minimum length is '.$this->schema['minLength'];
            }
        }
        if ((null !== ($this->schema['maxLength'] ?? null)) && !\is_object($value) && !\is_array($value)) {
            if (mb_strlen((string) $value) > $this->schema['maxLength']) {
                return 'Maximum length is '.$this->schema['maxLength'];
            }
        }

        if ((null !== ($this->schema['multipleOf'] ?? null)) && !\is_object($value) && !\is_array($value)) {
            $rem = (float) $value / (float) ($this->schema['multipleOf']);
            if ($rem !== floor($rem)) {
                return 'Value must be a multiple of '.$this->schema['multipleOf'];
            }
        }

        if (null !== ($this->schema['format'] ?? null)) {
            switch ($this->schema['format']) {
                case 'json':
                    break;

                case 'email':
                    $null = filter_var($value, \FILTER_VALIDATE_EMAIL, \FILTER_NULL_ON_FAILURE);
                    if (null === $null) {
                        return 'Invalid e-mail address';
                    }
                    break;

                case 'class-name':
                    if (!\is_string($value)) {
                        return 'Not a class name';
                    }
                    if (!class_exists($value)) {
                        return 'Class not found';
                    }
                    if ($null = ($this->schema['$parent'] ?? null)) {
                        if (!is_a($value, $null, true)) {
                            return "Class does not implement '$null'";
                        }
                    }
                    break;

                case 'date-time':
                    if (!\is_string($value)) {
                        return 'Not a date-time string';
                    }
                    $null = DateTime::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $value);
                    if (false !== $null) {
                        break;
                    }

                    $null = DateTime::createFromFormat("Y-m-d\TH:i:s.uP", $value);
                    if (false !== $null) {
                        break;
                    }

                    $null = DateTime::createFromFormat(DateTimeInterface::RFC3339, $value);
                    if (false !== $null) {
                        break;
                    }

                    $null = new DateTime($value);
                    if (false !== $null) {
                        break;
                    }

                    return 'Invalid date-time format';

                case 'date':
                    if (!\is_string($value)) {
                        return 'Not a date string';
                    }
                    $null = DateTime::createFromFormat('Y-m-d', $value);
                    if (false === $null) {
                        return 'Invalid date format';
                    }
                    break;

                case 'password':
                    break;

                case 'byte':
                    // base64
                    if (!\is_string($value)) {
                        return 'Not a string';
                    }
                    $null = base64_decode($value, true);
                    if (false === $null) {
                        return 'Invalid base64 encoded string';
                    }
                    break;

                case 'binary':
                    break;

                case 'uuid':
                    if (!\is_string($value)) {
                        return 'Not a string';
                    }
                    if (1 !== preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $value)) {
                        return 'Invalid UUID string';
                    }
                    break;

                case 'uri':
                    $null = filter_var($value, \FILTER_VALIDATE_URL, \FILTER_NULL_ON_FAILURE);
                    if (null === $null) {
                        return 'Invalid URI';
                    }
                    break;

                case 'http-url':
                    $null = filter_var($value, \FILTER_VALIDATE_URL, \FILTER_NULL_ON_FAILURE);
                    if (null === $null) {
                        return 'Invalid URL address';
                    }
                    $null = parse_url($null);
                    if ('http' !== ($null['scheme'] ?? null) && 'https' !== ($null['scheme'] ?? null)) {
                        return 'Invalid URL scheme';
                    }
                    break;

                case 'https-url':
                    $null = filter_var($value, \FILTER_VALIDATE_URL, \FILTER_NULL_ON_FAILURE);
                    if (null === $null) {
                        return 'Invalid URL address';
                    }
                    $null = parse_url($null);
                    if ('https' !== ($null['scheme'] ?? null)) {
                        return 'Not a https:// url';
                    }
                    break;

                case 'hostname':
                    /**
                     * @var string|false|null
                     */
                    $null = filter_var($value, \FILTER_VALIDATE_DOMAIN, \FILTER_NULL_ON_FAILURE | \FILTER_FLAG_HOSTNAME);
                    if (null === $null || false === $null) {
                        return 'Invalid hostname';
                    }
                    break;

                case 'ipv4':
                    /**
                     * @var string|false|null
                     */
                    $null = filter_var($value, \FILTER_VALIDATE_IP, \FILTER_NULL_ON_FAILURE | \FILTER_FLAG_IPV4);
                    /*
                     * @psalm-suppress TypeDoesNotContainNull
                     */
                    if (null === $null || false === $null) {
                        return 'Invalid IPv4 address';
                    }
                    break;

                case 'ipv6':
                    /**
                     * @var string|false|null
                     */
                    $null = filter_var($value, \FILTER_VALIDATE_IP, \FILTER_NULL_ON_FAILURE | \FILTER_FLAG_IPV6);
                    if (false === $null || null === $null) {
                        return 'Invalid IPv6 address';
                    }
                    break;

                case 'ip':
                    /**
                     * @var string|false|null
                     */
                    $null = filter_var($value, \FILTER_VALIDATE_IP, \FILTER_NULL_ON_FAILURE | \FILTER_FLAG_IPV6 | \FILTER_FLAG_IPV4);
                    if (null === $null || false === $null) {
                        return 'Invalid IP address';
                    }
                    break;
            }
        }

        if ((null !== ($this->schema['minProperties'] ?? null)) && (\is_array($value) || \is_object($value))) {
            if (\count((array) $value) < $this->schema['minProperties']) {
                return 'Object must have at least '.$this->schema['minProperties'].' properties';
            }
        }

        if ((null !== ($this->schema['maxProperties'] ?? null)) && (\is_array($value) || \is_object($value))) {
            if (\count((array) $value) > $this->schema['minProperties']) {
                return 'Object must have at most '.$this->schema['minProperties'].' properties';
            }
        }

        $propertyErrors = [];
        if (null !== ($this->schema['required'] ?? null)) {
            foreach ($this->schema['required'] as $null) {
                if (\is_object($value) && (!property_exists($value, $null) || empty($value->$null))) {
                    $propertyErrors[$null] = 'Required';
                } elseif (\is_array($value) && (!\array_key_exists($null, $value) || empty($value[$null]))) {
                    $propertyErrors[$null] = 'Required';
                }
            }
        }

        $extraProperties = [];
        if ((null !== ($this->schema['properties'] ?? null)) && (\is_object($value) || \is_array($value))) {
            $extraProperties = (array) $value;
            foreach ($this->schema['properties'] as $propertyName => $propertySchema) {
                unset($extraProperties[$propertyName]);

                if (isset($propertyErrors[$propertyName])) {
                    continue;
                }

                if (\is_object($value) && property_exists($value, $propertyName) && !empty($value->$propertyName)) {
                    $propertyValue = $value->$propertyName;
                } elseif (\is_array($value) && \array_key_exists($propertyName, $value) && !empty($value[$propertyName])) {
                    $propertyValue = $value[$propertyName];
                } else {
                    // no validation if the value is not submitted
                    continue;
                }

                $propertyValidator = new self($propertySchema + []);
                $propertyError = $propertyValidator->isInvalid($propertyValue);
                if (null !== $propertyError) {
                    $propertyErrors[$propertyName] = $propertyError;
                }
            }
        }

        if (!($this->schema['additionalProperties'] ?? null) && \count($extraProperties) > 0) {
            foreach ($extraProperties as $propertyName => $null) {
                $propertyErrors[$propertyName] = 'Illegal additional property';
            }
        }

        if (\count($propertyErrors) > 0) {
            return $propertyErrors;
        }

        if ((null !== ($this->schema['minItems'] ?? null)) && \is_array($value)) {
            if (\count($value) < $this->schema['minItems']) {
                return 'Array must contain at least '.$this->schema['minItems'].' items';
            }
        }

        if ((null !== ($this->schema['maxItems'] ?? null)) && \is_array($value)) {
            if (\count($value) > $this->schema['maxItems']) {
                return 'Array must contain at most '.$this->schema['maxItems'].' items';
            }
        }

        if ((true === ($this->schema['uniqueItems'] ?? null)) && \is_array($value)) {
            $seen = [];
            foreach ($value as $item) {
                if (\in_array($item, $seen)) {
                    return 'Duplicate item '.json_encode($item).' in array';
                }
                $seen[] = $item;
            }
        }

        if (null !== ($pattern = ($this->schema['pattern'] ?? null))) {
            $count = preg_match('/'.$pattern.'/', $value, $matches);
            if ($count < 1) {
                return "Value does not match pattern '$pattern'";
            }
        }

        $itemErrors = [];
        if ((null !== ($this->schema['items'] ?? null)) && \is_array($value)) {
            $itemValidator = new self($this->schema['items']);
            foreach ($value as $itemIndex => $item) {
                if (!\is_int($itemIndex)) {
                    continue;
                }
                $itemError = $itemValidator->isInvalid($item);
                if (null !== $itemError) {
                    $itemErrors[$itemIndex] = $itemError;
                }
            }
        }
        if (\count($itemErrors) > 0) {
            return $itemErrors;
        }

        return null;
    }
}
