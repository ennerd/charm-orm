<?php
declare(strict_types=1);

namespace Charm\ORM;

use Charm\Context\Context;

/**
 * This class holds state which is accessible for all classes extending it.
 */
class Base
{
    /**
     *  @var array{
     *      dirty: bool,
     *      from_database: bool,
     *      in_database: bool,
     *      values: array<string, mixed>,
     *      values_original: array<string, mixed>,
     *      transaction_tag: float|null
     *  }
     */
    protected $data = [
        // Has the object been modified since it was last created, loaded or saved?
        'dirty' => false,

        // Did the object originally come from the database?
        'from_database' => false,

        // Does the object currently exist in the database?
        'in_database' => false,

        /*
         * Contains the values for all declared properties
         */
        'values' => [],

        /*
         * Contains original values for columns that have changed, when
         * the values came from the database.
         */
        'values_original' => [],

        /**
         * Contains the transaction tag from within this row was
         * loaded.
         */
        'transaction_tag' => null
    ];

    /**
     * @var array<string, mixed>
     */
    protected static array $shared = [];

    /**
     * Call this method for every row that came from the database.
     *
     * @return $this
     */
    protected function markFromDatabase()
    {
        $this->data['from_database'] = true;
        $this->data['in_database'] = true;
        $ctx = Context::instance();
        if (!empty($ctx->db->transactionTag)) {
            $this->data['transaction_tag'] = $ctx->db->transactionTag;
            if (!isset($ctx->ormTransactionObjects)) {
                $ctx->ormTransactionObjects = [];
                $ctx->ormTransactionObjects[] = $this;
            }
        }

        return $this;
    }

    /**
     * Call this method for every row after it was saved to the database.
     *
     * @param array<string> $onlyTheseColumns
     *
     * @return $this
     */
    protected function markSaved(array $onlyTheseColumns = null)
    {
        if (null !== $onlyTheseColumns) {
            foreach ($onlyTheseColumns as $column) {
                unset($this->data['values_original'][$column]);
            }
            $this->data['dirty'] = \count($this->data['values_original']) > 0;
        } else {
            $this->data['values_original'] = [];
            $this->data['dirty'] = false;
        }
        $this->data['in_database'] = true;

        return $this;
    }

    /**
     * @return $this
     */
    protected function markDeleted()
    {
        $this->data['saved'] = false;
        $this->data['in_database'] = false;

        return $this;
    }
}
