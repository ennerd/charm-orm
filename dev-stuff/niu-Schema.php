<?php
declare(strict_types=1);

namespace Charm\ORM;

/**
 * Describe the structure and metadata for a single unit of information, such as
 * a 'user', or a 'contact card'.
 *
 * Schema definition is done through an array structure:
 *
 *  [
 *      'type' => 'array'|'object'|'integer'|'number'|'string'|'boolean',
 *
 *      // When type='object':
 *      'properties' => [
 *          'propertyName' => Model,
 *      ]
 *
 * @property string $id;
 * @property string $title
 * @property string $description
 * @property mixed  $default
 * @property array  $examples
 * @property bool   $readOnly
 * @property bool   $writeOnly
 * @property array  $enum
 * @property mixed  $const
 * @property self[] $allOf
 * @property self[] $anyOf
 * @property self[] $oneOf
 * @property self   $not
 * @property string $type
 */
class Schema
{
    public const SCHEMA_SCHEMA = [
        'required' => [
            'type',
        ],
        'properties' => [
            'type' => [
                'type' => self::TYPE_STRING,
                'enum' => [
                    self::TYPE_NULL,
                    self::TYPE_STRING,
                    self::TYPE_INTEGER,
                    self::TYPE_NUMBER,
                    self::TYPE_BOOLEAN,
                    self::TYPE_OBJECT,
                    self::TYPE_ARRAY,
                ],
            ],

            'allOf' => null,
            'allOf' => null,
            'oneOf' => null,
            'anyOf' => null,
            'not' => null,
            'items' => null,
            'additionalProperties' => null,
            'description' => [
                'type' => 'string',
            ],
            'format' => [
                'type' => 'string',
            ],
            'default' => [],
            'title' => [
                'type' => self::TYPE_STRING,
            ],
            'multipleOf' => null,
            'properties',
            'maximum' => [
                'type' => self::TYPE_INTEGER,
            ],
            'exclusiveMaximum',
            'minimum' => [
                'type' => self::TYPE_INTEGER,
            ],
            'exclusiveMinimum' => [
                'type' => self::TYPE_BOOLEAN,
            ],

            'maxLength' => [
                'type' => self::TYPE_INTEGER,
            ],

            'minLength' => [
                'type' => self::TYPE_INTEGER,
            ],

            'pattern' => [
                'type' => self::TYPE_STRING,
            ],

            'maxItems' => [
                'type' => self::TYPE_INTEGER,
            ],

            'minItems' => [
                'type' => self::TYPE_INTEGER,
            ],

            'uniqueItems' => [
                'type' => self::TYPE_BOOLEAN,
            ],

            'maxProperties' => [
                'type' => self::TYPE_INTEGER,
            ],

            'minProperties' => [
                'type' => self::TYPE_INTEGER,
            ],

            'required' => [
                'type' => self::TYPE_ARRAY,
                'items' => [
                    'type' => self::TYPE_STRING,
                ],
            ],

            'enum' => [
                'type' => self::TYPE_ARRAY,
                'items' => [
                    'type' => self::TYPE_STRING,
                ],
            ],
        ],
    ];

    public const TYPE_NULL = 'null';
    public const TYPE_STRING = 'string';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_NUMBER = 'number';
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_OBJECT = 'object';
    public const TYPE_ARRAY = 'array';

    public function __construct(mixed $schema, array $schemaSchema = self::SCHEMA_SCHEMA)
    {
    }

    public function validate(mixed $value): ?string
    {
    }

    /**
     * Validate a value against a schema definition.
     *
     * @param array  $definition      An array structure that complies with OpenAPI 3.0
     * @param mixed  $value           A value to check against the definition
     * @param string $valueIdentifier A string used in error messages to identify the value
     *
     * @return void
     */
    protected static function _validate(array $definition, mixed &$value, string $valueIdentifier = 'value'): ?string
    {
        // More legal properties will be added depending on type
        $legalProperties = [
            'title',
            'description',
            'default',
            'examples',
            'readOnly',
            'writeOnly',
            'enum',
            'const',
            'allOf',
            'anyOf',
            'oneOf',
            'not',

            'type', // 'type' is added here to simplify schema validation

            '$comment',
            '$ref',
            '$id',
        ];

        // If we don't have a value, use the default
        if (null === $value) {
            if (\array_key_exists('default', $definition)) {
                $value = $definition['default'];
            }
        }

        // More required properties may be added depending on type. The 'type' property is always required.
        $requiredProperties = [
        ];

        // All definitions must have a type definition
        if (!\array_key_exists('type', $definition)) {
            throw new Error("definition.type is required when validation '$valueIdentifier'");
        }

        // This is all about validating the schema and the value type
        switch ($definition['type']) {
            case static::TYPE_NULL:
                if (null !== $value) {
                    return "'$valueIdentifier' must be null";
                }
                break;
            case static::TYPE_BOOLEAN:
                if (!\is_bool($value)) {
                    return "'$valueIdentifier' must be a boolean value";
                }
                break;
            case static::TYPE_INTEGER:
                if (!\is_int($value)) {
                    return "'$valueIdentifier' must be an integer value";
                }
                $legalProperties[] = 'minimum';
                $legalProperties[] = 'maximum';
                $legalProperties[] = 'exclusiveMinimum';
                $legalProperties[] = 'exclusiveMaximum';
                $legalProperties[] = 'multipleOf';
                break;
            case static::TYPE_NUMBER:
                if (!\is_int($value) && !\is_float($value)) {
                    return "'$valueIdentifier' must be a number value";
                }
                $legalProperties[] = 'minimum';
                $legalProperties[] = 'maximum';
                $legalProperties[] = 'exclusiveMinimum';
                $legalProperties[] = 'exclusiveMaximum';
                $legalProperties[] = 'multipleOf';
                break;
            case static::TYPE_STRING:
                if (!\is_string($value)) {
                    return "'$valueIdentifier' must be a string value";
                }
                $legalProperties[] = 'minLength';
                $legalProperties[] = 'maxLength';
                $legalProperties[] = 'pattern';
                $legalProperties[] = 'format';
                $legalProperties[] = 'contentMediaType';
                $legalProperties[] = 'contentEncoding';
                break;
            case static::TYPE_ARRAY:
                if (!\is_array($value)) {
                    return "'$valueIdentifier' must be an array";
                }
                if (array_keys($value) !== range(0, \count($value) - 1)) {
                    return "'$valueIdentifier' must be a simple array but it is an associative array";
                }
                $legalProperties[] = 'items';
                $legalProperties[] = 'minItems';
                $legalProperties[] = 'maxItems';
                $legalProperties[] = 'uniqueItems';
                break;
            case static::TYPE_OBJECT:
                if (\is_array($value)) {
                    foreach ($value as $k => $v) {
                        if (\is_int($k)) {
                            return "'$valueIdentifier' must be an object or an associative array with no integer keys";
                        }
                    }
                    $value = (object) $value;
                } elseif (!\is_object($value)) {
                    return "'$valueIdentifier' must be an object or an associative array";
                    throw new Error('Expected an object or an associative array value');
                }
                $legalProperties[] = 'properties';
                $legalProperties[] = 'patternProperties';
                $legalProperties[] = 'additionalProperties';
                $legalProperties[] = 'required';
                $legalProperties[] = 'minProperties';
                $legalProperties[] = 'maxProperties';
                break;
            default:
                throw new Error("Unknown type '".$definition['type']."'. Expects any of the Model::TYPE_* types");
        }

        /*
         * First check that all required properties of the schema is defined in the schema
         */
        foreach ($requiredProperties as $propertyName) {
            if (!\array_key_exists($propertyName, $definition)) {
                throw new Error("Schema for validating '$valueIdentifier' does not have a '$propertyName' property");
            }
        }

        /*
         * Then check that the definition does not have any properties that are illegal
         */
        foreach ($definition as $propertyName => $null) {
            if (!\in_array($propertyName, $legalProperties)) {
                throw new Error("Schema for validating '$valueIdentifier' does not allow '$propertyName'");
            }
        }

        /*
         * Check each property
         */
        foreach ($definition as $propertyName => $propertyDefinition) {
            /*
             * Eat our own dog food to validate properties in the definition
             */
            if (\array_key_exists($propertyName, static::SCHEMA_SCHEMA['properties'])) {
                $schemaError = static::_validate(static::SCHEMA_SCHEMA['properties'][$propertyName], $propertyDefinition, "schema for $valueIdentifier.$propertyName");
                if (null !== $schemaError) {
                    return $schemaError;
                }
            }

            /*
             * Implement functionality behind the properties that can be used in the schema
             */
            switch ($propertyName) {
                case 'default':
                case 'examples':
                case 'readOnly':
                case 'writeOnly':
                case 'type':
                case '$comment':
                    break;
                case 'enum':
                    /*
                     * Value can only be one of the enum specified values
                     */
                    if (!\in_array($value, $propertyDefinition)) {
                        return "'$valueIdentifier' must be one of: ".implode(', ', $propertyDefinition);
                    }
                    break;
                case 'const':
                    /*
                     * Value can only be this exact value
                     */
                    if ($value !== $propertyDefinition) {
                        return "'$valueIdentifier' must be the value ".json_encode($propertyDefinition);
                    }
                    break;
                case 'allOf':
                    throw new Error("'allOf' is not supported yet");
                case 'anyOf':
                    throw new Error("'anyOf' is not supported yet");
                case 'oneOf':
                    throw new Error("'oneOf' is not supported yet");
                case 'not':
                    throw new Error("'not' is not supported yet");
                    break;
                case 'contentMediaType':
                    throw new Error("'contentMediaType' is not supported yet");
                case 'contentEncoding':
                    throw new Error("'contentEncoding' is not supported yet");
                case '$ref':
                    throw new Error("'\$ref' is not supported yet");
                case '$id':
                    throw new Error("'\$id' is not supported yet");
                case 'required':
                    foreach ($propertyDefinition as $requiredPropertyName) {
                        if (!property_exists($value, $requiredPropertyName)) {
                            return "'$valueIdentifier.$requiredPropertyName' is required";
                        }
                    }
                    break;
                case 'properties':
                    /*
                     * type="object" can declare which properties they have
                     */
                    foreach ($propertyDefinition as $subPropertyName => $subSchema) {
                        /*
                         * No need to perform validation
                         */
                        if (!\array_key_exists('default', $subSchema) && !property_exists($value, $subPropertyName)) {
                            continue;
                        }
                        $subError = static::_validate($subSchema, $value->$subPropertyName, "$valueIdentifier.$subPropertyName");
                        if (null !== $subError) {
                            return $subError;
                        }
                    }
                    break;
                case 'minimum':
                    if ($value < $propertyDefinition) {
                        return "'$valueIdentifier' must be minimum $propertyDefinition";
                    }
                    break;
                case 'exclusiveMinimum':
                    if ($value <= $propertyDefinition) {
                        return "'$valueIdentifier' must be greater then $propertyDefinition";
                    }
                    break;
                case 'maximum':
                    if ($value > $propertyDefinition) {
                        return "'$valueIdentifier' must be maximum $propertyDefinition";
                    }
                    break;
                case 'exclusiveMaximum':
                    if ($value >= $propertyDefinition) {
                        return "'$valueIdentifier' must be less then $propertyDefinition";
                    }
                    break;
                case 'multipleOf':
                    if ($value / $propertyDefinition !== floor($value / $propertyDefinition)) {
                        return "'$valueIdentifier' must be a multiple of $propertyDefinition";
                    }
                    break;
                case 'minLength':
                    if (mb_strlen($value) < $propertyDefinition) {
                        return "'$valueIdentifier' must be at least $propertyDefinition characters long";
                    }
                    break;
                case 'maxLength':
                    if (mb_strlen($value) > $propertyDefinition) {
                        return "'$valueIdentifier' must be at most $propertyDefinition characters long";
                    }
                    break;
                case 'pattern':
                    $regexSeparator = '/~|$';
                    for ($i = 0; $i < \strlen($regexSeparator); ++$i) {
                        if (false === strpos($propertyDefinition, $regexSeparator[$i])) {
                            $regexSeparator = $regexSeparator[$i];
                            break;
                        }
                    }
                    if (1 !== \strlen($regexSeparator)) {
                        throw new Error("Could not find a usable regex separator for '$valueIdentifier");
                    }
                    $filtered = filter_var($value, \FILTER_VALIDATE_REGEXP, ['regexp' => $regexSeparator.$propertyDefinition.$regexSeparator]);
                    if ($filtered !== $value) {
                        return "'$valueIdentifier' does not match pattern '$propertyDefinition'";
                    }
                    break;
                case 'format':
                    switch ($propertyDefinition) {
                        case 'date-time':
                        case 'date':
                        case 'time':
                        case 'email':
                        case 'idn-email':
                        case 'hostname':
                        case 'idn-hostname':
                        case 'ipv4':
                        case 'ipv6':
                        case 'uri':
                        case 'uri-reference':
                        case 'iri':
                        case 'iri-reference':
                        case 'uri-template':
                        case 'json-pointer':
                        case 'relative-json-pointer':
                        case 'regex':
                        default:
                            throw new Error("'format=$propertyDefinition' is not supported yet");
                    }
            }
        }
    }
}
